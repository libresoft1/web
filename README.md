# libresoft web

Sitio web de Libresoft Bolivia.

Tecnologías usadas:

- NodeJS - https://nodejs.org
- Vue.js - http://vuejs.org/
- Nuxt.js - https://nuxtjs.org/
- Vuetify - https://vuetifyjs.com/
- Font Awesome 4 - https://fontawesome.com/v4.7.0/
- Axios - https://github.com/axios/axios
- git-flow - https://github.com/nvie/gitflow


Licencia: AGPLv3
Autor: Rodrigo Garcia Saenz <rgarcia@laotra.red> 2021
