const fs = require('fs');
const path = require('path');
import webpack from 'webpack';
import colors from 'vuetify/es5/util/colors'

const pkg = require('./package');
const cfg = JSON.parse(fs.readFileSync('./config.json'));

export default {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - Libresoft Bo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Sitio web de Libresoft Bo',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/main.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
    '~/plugins/dayjs',
    '~/plugins/error',
    '~/plugins/error-handler',
    '~/plugins/font-awesome-icons',
    '~/plugins/dialog-confirm',
    '~/plugins/i18n-extra',
    '~/plugins/success',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    ['nuxt-i18n', {
      // locales: ['en', 'es'],
      // defaultLocale: 'es',
      locales: [
        { code: 'en', iso: 'en-US', file: 'en-US.js' },
        { code: 'es', iso: 'es-ES',file: 'es-ES.js' },
      ],
      defaultLocale: 'es',
      lazy: true,
      langDir: 'lang/',
      detectBrowserLanguage: {
        // If enabled, a cookie is set once a user has been redirected to his
        // preferred language to prevent subsequent redirections
        // Set to false to redirect every time
        useCookie: true,
        // Cookie name
        cookieKey: 'i18n_redirected',
        // Set to always redirect to value stored in the cookie, not just once
        alwaysRedirect: false,
        // If no locale for the browsers locale is a match, use this one as a fallback
        fallbackLocale: 'es',
      },
      redirectCookieKey: 'redirected',
      useRedirectCookie: 'en',
    }],
    ['nuxt-leaflet', {}],
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: cfg.baseUrl,
  },
  router: {
    // folder in which the app will run
    base: cfg.subDomain,
    mode: 'history',
    // middleware: 'check-auth',
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    defaultAssets: false,
    icons: {
      iconfont: 'fa4',
    },
    theme: {
      // dark: true,
      light: true,
      themes: {
        light: {
          primary: '#0aa699',
          secondary: '#424242',
          accent: '#82B1FF',
          error: '#FF5252',
          info: '#1F3853',
          success: '#0090d9',
          warning: '#FFC107',
          // primary: colors.blue.darken2,
          // accent: colors.grey.darken3,
          // secondary: colors.amber.darken3,
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          // error: colors.deepOrange.accent4,
          // success: colors.green.accent3,
        },
      },
    },
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },
  },

  srcDir: 'src/',
  ...(cfg.https
    ? {
      server: {
        port: 8080,
        https: {
          key: fs.readFileSync(path.resolve(__dirname, 'ejemplo-key.pem')),
          cert: fs.readFileSync(path.resolve(__dirname, 'ejemplo-cert.pem')),
        },
      },
    }
    : {}),
}
