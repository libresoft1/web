const handleError = {
  /**
   * Utilitario para manejo de request errors de axios, e.g.:
   * this.$axios.post('/ruta')
   * .then((response) => { 'ok' })
   * .catch(this.$handleError.request((errors) => {
   *   console.log(errors);
   * }))
   */
  request (...args) {
    // si se envia 2 argumentos, (las claves del objeto donde se almacenan los errores y el callback
    // de manejo por cada error que se puede agregar a el objeto de errores) e.g.:
    // .catch(this.$handleError.request(['email', 'nombre'], (err) => {
    //   this.errors[err.field].push(err.code);
    // }))
    if (args.length > 1) {
      return (error) => {
        if (error.response && `${error.response.status}`.startsWith('4')) {
          error.response.data.errors.forEach((err) => {
            if (args[0].includes(err.field)) {
              args[args.length - 1](err);
            }
          });
          return;
        }
        throw error;
      };
    }
    // si solo se envia el callback de manejo de todos los errores en general
    if (args.length > 0) {
      const errorCallback = args[0];
      return (error) => {
        if (error.response && `${error.response.status}`.startsWith('4')) {
          return errorCallback(error.response.data.errors);
        }
        throw error;
      };
    }
    // si no tiene argumentos, entonces es un error del usuario y solo propaga el error
    return (error) => {
      throw error;
    };
  },
};

export default (context, inject) => {
  // Inject $handleError(msg) in Vue, context and store.
  inject('handleError', handleError)
  // For Nuxt <= 2.12, also add 👇
  context.$handleError = handleError
}
