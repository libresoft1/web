import Vue from 'vue';

const successShow = (mensaje, context) => {
  context.store.commit('message/MOSTRAR', {
    text: mensaje,
    type: 'success',
  });
  return null;
};

export default (context, inject) => {
  const showSuccess = (msj) => {
    successShow(msj, context);
  };
  inject('showSuccess', showSuccess);
  context.$showSuccess = showSuccess;
};
