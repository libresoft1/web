import Vue from 'vue';
const dayjs = require('dayjs');
const customParseFormat = require('dayjs/plugin/customParseFormat')
const isSameOrBefore = require('dayjs/plugin/isSameOrBefore')
const isSameOrAfter = require('dayjs/plugin/isSameOrAfter')

dayjs.extend(customParseFormat);
dayjs.extend(isSameOrBefore);
dayjs.extend(isSameOrAfter);

// los metodos siempre deberian devolver un dayjs para facilitar la extensibilidad a menos que se
// quiera devolver un ISO, para formatear los valores se debe usar la funcion format de la instancia
// o de este mismo plugin
function now () {
  return dayjs();
}

function nowToISO () {
  return now().format('YYYY-MM-DD');
}

function parse (date, format = 'DD/MM/YYYY') {
  const parsed = dayjs(date, format);
  if (!parsed.isValid()) return null;

  return parsed;
}

// anterior format
function parseAndFormat (date, formatIn = 'YYYY-MM-DD', formatOut = 'DD/MM/YYYY') {
  const parsed = parse(date, formatIn);
  if (!parsed) return null;

  return parsed.format(formatOut);
}

function parseToISO (date, format = 'DD/MM/YYYY') {
  return parseAndFormat(date, format, 'YYYY-MM-DD');
}

// quitando dateISO por parseToISO
function dateISO (date) {
  return parseToISO(date, null);
}

function add (date, key, value) {
  const parsed = dayjs(date);
  if (!parsed.isValid()) return null;

  return parsed.add(value, key);
}

function subtract (date, key, value) {
  const parsed = dayjs(date);
  if (!parsed.isValid()) return null;

  return parsed.subtract(value, key);
}

function isBefore (date1, date2, same = false) {
  return same ? date1.isSameOrBefore(date2) : date1.isBefore(date2);
}

function isAfter (date1, date2, same = false) {
  return same ? date1.isSameOrAfter(date2) : date1.isAfter(date2);
}

Vue.prototype.$dayjs = {
  now,
  nowToISO,
  parse,
  parseAndFormat,
  parseToISO,
  dateISO,
  add,
  subtract,
  isBefore,
  isAfter,
};
