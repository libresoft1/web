import Vue from 'vue';

const translateWithArgs = (text, args) => {
  return args.reduce((acc, arg, idx) => {
    return acc.replace(new RegExp(`\\{${idx}\\}`, 'g'), arg);
  }, text);
};

export default (context, inject) => {
  // NOTE: este método no funciona porque context.$t no está definido
  const t2 = (code, args = []) => {
    return translateWithArgs(context.$t(code), args);
  };

  inject('t2', t2);
  context.$t2 = t2;

  const tt = translateWithArgs;

  inject('tt', tt);
  context.$tt = tt;
}
