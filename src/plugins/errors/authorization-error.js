import ApiError from './api-error';

class AuthorizationError extends ApiError {
  constructor(message, args = []) {
    super('AuthorizationError', message || 'Error de Autorización');
    this.data = args;
  }

  getBody() {
    return [{
      code: 'authorization.token.missing.error',
      field: 'token',
      value: this.data,
    }];
  }
}

export default AuthorizationError;
