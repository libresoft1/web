import ApiError from './api-error';

class SimpleError extends ApiError {
  constructor(message) {
    super('SimpleError', message || 'Error de Aplicación');
    this.data = {};
  }

  getBody() {
    return {};
  }

  static handle(callback) {
    return (err) => {
      if (err instanceof SimpleError) {
        if (typeof callback === 'function') {
          return callback(err);
        }
        return callback;
      }
      throw err;
    };
  }
}

export default SimpleError;
