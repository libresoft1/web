export default function ({ $axios, redirect, store }) {
  $axios.onResponse((response) => {
    store.commit('message/LIMPIAR');
  });

  $axios.onError((error) => {
    const code = parseInt(error.response && error.response.status);
    if (code && `${code}`.startsWith('5')) {
      // si es un error de servidor
      // mostrar un mensaje arriba de falla de servidor
      store.commit('message/MOSTRAR', {
        text: 'Error del servidor',
        type: 'error',
      });
    } else if (!code) {
      // si es falla de conexion
      // mostrar un mensaje arriba de falla de conexion
      store.commit('message/MOSTRAR', {
        text: 'Error de conexión',
        type: 'error',
      });
    }
    throw error;
  });
};
