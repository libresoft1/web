import Vue from "vue";
import VuetifyDialogPromise from "vuetify-dialog-promise";

Vue.use(VuetifyDialogPromise, {
  locale : 'es',
  snackbarX : "left",
  snackbarY : "bottom",
});
