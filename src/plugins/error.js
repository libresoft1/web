import Vue from 'vue';

import ApiError from './errors/api-error';
import ValidationError from './errors/validation-error';
import FieldError from './errors/field-error';
import NotFoundError from './errors/not-found-error';
import AuthorizationError from './errors/authorization-error';
import SimpleError from './errors/simple-error';

Vue.prototype.$appError = {
  ValidationError,
  FieldError,
  NotFoundError,
  AuthorizationError,
  SimpleError,
};

const errorShowAndThrow = (err, context, keepChain = true) => {
  context.store.commit('message/MOSTRAR', {
    text: err.message,
    type: 'error',
  });
  // keeps throwing error chain
  if (keepChain) {
    throw err;
  }
  return null;
};

export default (context, inject) => {
  const showError = (msg, keepChain) => {
    if (msg instanceof Error) {
      return errorShowAndThrow(msg, context, keepChain);
    }
    return (err) => {
      return errorShowAndThrow(new Error(msg), context, keepChain);
    };
  }
  // Inject $handleError(msg) in Vue, context and store.
  inject('showError', showError)
  // For Nuxt <= 2.12, also add 👇
  context.$showError = showError
}
